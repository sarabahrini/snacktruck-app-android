package com.sarabahrini.snacktruckapp;

/*********************************************************************************
Abstract class for API calls dummy functions. Sending order and adding items return
 true. Getting menu returns a JASON object with default values.
 *********************************************************************************/

public abstract class APICalls {

    public static boolean sendOrder(String order){
        return true;
    }

    public static boolean addItem(String item){
        return true;
    }

    public static String getMenu(){
        //Replace return string with api call.
//        return ("Veggie\n" +
//                "French fries\n" +
//                "Veggieburger\n" +
//                "Carrots\n" +
//                "Apple\n" +
//                "Banana\n" +
//                "Milkshake\n" +
//                "Non-veggie\n" +
//                "Cheeseburger\n" +
//                "Hamburger\n" +
//                "Hot dog");

        return ("{\"MenuItems\":[{\"name\": \"French fries\",\"isVeggie\":true }, {\"name\": \"Carrots\",\"isVeggie\": true}," +
                " {\"name\": \"Apple\",\"isVeggie\": true }, {\"name\": \"Banana\",\"isVeggie\": true },"
                + "{\"name\": \"Milkshake\",\"isVeggie\": true }, {\"name\": \"Hamburger\",\"isVeggie\": false"
                + "},{\"name\": \"Cheeseburger\",\"isVeggie\": false },{\"name\": \"Hot dog\",\"isVeggie\": false }]}");

        }

    }


