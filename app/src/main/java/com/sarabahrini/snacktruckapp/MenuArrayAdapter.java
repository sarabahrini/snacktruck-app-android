package com.sarabahrini.snacktruckapp;

/*********************************************************************************
The menuArrayAdapter class defined here. This class stores the homepage items and defines their behavior.
 *********************************************************************************/
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import java.util.ArrayList;

public class MenuArrayAdapter extends ArrayAdapter<MenuItem> {

    private static class ViewHolder{
        TextView name;
        CheckBox selected;
    }

    public MenuArrayAdapter(Context context, ArrayList<MenuItem> menuItems){
        super(context, R.layout.menu_item, menuItems);
    }

    public void setSelected(int position, boolean selected){
    }

    private boolean showVeggie = true;
    private boolean showNonVeggie = true;

    //uncommenting the four following line of code will enable veggie toggle.
    // The filters work for the first change but the app crashes when switched back on

    public void setShowNonVeggie(boolean showNonVeggie) {
        //if (!showNonVeggie)
            //this.setFilter(showVeggie, showNonVeggie);
    }

    public void setShowVeggie(boolean showVeggie) {
        //if (!showVeggie)
            //this.setFilter(showVeggie, showNonVeggie);
    }

    public void setFilter(boolean showVeggie, boolean showNonVeggie){
        this.showVeggie = showVeggie;
        this.showNonVeggie = showNonVeggie;

        notifyDataSetChanged();
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        MenuItem item = getItem(position);
        final ViewHolder viewHolder;
        if (view == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            view = inflater.inflate(R.layout.menu_item, parent,false);
            viewHolder.name = view.findViewById(R.id.textView);
            viewHolder.selected = view.findViewById(R.id.checkbox);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        if ((!showVeggie && item.getVegeterian()) || (!showNonVeggie && !item.getVegeterian())) {
            return new View(getContext());
        }

        viewHolder.name.setText(item.getName());
        viewHolder.selected.setChecked(item.getSelected());
        viewHolder.selected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    setSelected(position,viewHolder.selected.isChecked());
            }
        });

            viewHolder.name.setTextColor(item.getColor(getContext().getResources()));
            return view;
        }

}
