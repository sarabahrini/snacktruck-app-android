package com.sarabahrini.snacktruckapp;
/*********************************************************************************
Menu item class
 *********************************************************************************/


import android.content.res.Resources;
import android.support.v4.content.res.ResourcesCompat;


public class MenuItem  {


    public String getName() {
        return name;
    }

    public boolean getVegeterian() {
        return vegeterian;
    }

    public boolean getSelected(){
        return selected;
    }

    public void setSelected(boolean selected){
        this.selected=selected;
    }

    public int getColor(Resources resources){

        if (this.vegeterian){
            return ResourcesCompat.getColor(resources, R.color.colorVeggie, null);
        }
        return ResourcesCompat.getColor(resources, R.color.colorNonVeggie, null);


    }

     private String name;
     private boolean vegeterian;
     private boolean selected;

     //constructor taking name and if vegetarian
    public MenuItem(String name, boolean vegeterian){
        this.name=name;
        this.vegeterian=vegeterian;
        this.selected=selected;

    }



}
