package com.sarabahrini.snacktruckapp;

/*********************************************************************************
Following functionalities implemented in this file:
    * Add item functionalities
    * Submit order
    * Show order summary
    * Veggie/Non-Veggie filter toggles (Not implemented completely)
 *********************************************************************************/

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import java.util.ArrayList;
import org.json.JSONArray ;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    ArrayList <MenuItem> menuItems;
    MenuArrayAdapter adapter;
    //Load main component, default view
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ListView listView = findViewById(R.id.menu);

        menuItems = new ArrayList<>(parseMenu(APICalls.getMenu()));

        //Adapter holds the menu items
        adapter = new MenuArrayAdapter(this, menuItems){
            //Overrides base setSelected function
            @Override
            public void setSelected(int position, boolean selected){
                menuItems.get(position).setSelected(selected);
            }
        };
        listView.setAdapter(adapter);

        //Set default veggie and non-veggie checkBox to true
        adapter.setFilter(true, true);

        //Register a listener for the veggie compound button
        ((CheckBox)findViewById(R.id.cb_Veggie)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                adapter.setShowVeggie(isChecked);
            }
        });

        //Register a listener for the non-veggie compound button
        ((CheckBox)findViewById(R.id.cb_NotVeggie)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                adapter.setShowNonVeggie(isChecked);
            }
        });

        Button addButton = findViewById(R.id.btnAdd);
        // on add click button showInput Dialog
        addButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                showInputDialog();
            }
        });

        Button submitButton = findViewById(R.id.btnSubmit);
        // On submit click button and console log the status
        submitButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                //Verify order validity
                String message = "";
                for (int i = 0; i < menuItems.size(); i++) {
                    MenuItem menuItem = menuItems.get(i);
                    if (menuItem.getSelected()) {
                        message += menuItems.get(i).getName() + "\n";
                    }
                }
                if (APICalls.sendOrder(message)) {
                    Log.e("FragmentActivity", "Order Sent Successfully!");
                    showSummary(message);
                }
                else
                    Log.e("FragmentActivity", "Order did not send successfully!");
            }
        });
    }

    //Parse received string to get menu items and if veggie
    private ArrayList<MenuItem> parseMenu(String menu) {
        try {

            ArrayList <MenuItem> menuItems= new ArrayList<>();
            JSONObject obj = new JSONObject(menu);
            JSONArray JArr = obj.getJSONArray("MenuItems");
            for (int i= 0; i < JArr.length(); i++) {
                menuItems.add(new MenuItem(JArr.getJSONObject(i).getString("name")
                        ,(JArr.getJSONObject(i).getBoolean("isVeggie"))));
            }
            return (menuItems);
        }
        catch (Exception e)
        {
            return null;
        }

    }

    private void submit(){
        for (int i = 0; i < menuItems.size(); i++) {
            menuItems.get(i).setSelected(false);
        }
        adapter.notifyDataSetChanged();
    }

    //Create string of orders and print in the summary pop up
    private void showSummary(String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Order Summary");

        builder.setMessage(message);
        builder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which){
                dialog.cancel();
                submit();
            }
        });
        builder.show();
    }

    //Display dialog with text edit for adding new menu item
    protected void showInputDialog() {
        LayoutInflater layoutInflater = LayoutInflater.from(MainActivity.this);
        View promptView = layoutInflater.inflate(R.layout.input_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
        alertDialogBuilder.setView(promptView);

        final EditText editText = (EditText) promptView.findViewById(R.id.edittext);
        final CheckBox checkbox = (CheckBox) promptView.findViewById(R.id.checkbox2);

        alertDialogBuilder.setCancelable(false);
        //Dialog save button - does not save duplicated items
        alertDialogBuilder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        boolean ifExist = false;
                        for(int i =0; i < menuItems.size(); i++)
                        {
                            if (menuItems.get(i).getName().equals(editText.getText().toString())) {
                                ifExist = true;
                                break;
                            }
                        }
                        if (!ifExist) {
                            if (APICalls.addItem(editText.getText().toString())) {
                                MenuItem menuItem = new MenuItem(editText.getText().toString(), checkbox.isChecked());
                                menuItems.add(menuItem);
                                Log.e("FragmentActivity", "New item is added successfully!");
                            } else
                                Log.e("FragmentActivity", "ERROR adding item!");
                        }
                        else
                            Log.e("FragmentActivity", "Item already exists!");

                    }
                });
        //Dialog cancel button
        alertDialogBuilder.setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        // Show and create the dialog with the defined details above
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }
}
