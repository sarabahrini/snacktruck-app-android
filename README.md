Priority one: Completed
Priority two: UI completed, buggy performance, disabled currently
Priority three: Completed

Project includes 4 layout XML files and 4 Java code files. 2 of the files are default (mainActivity, Activity_Main).
The three non-default Java files include two main class definitions and methods, and one abstract class for API calls. 
MenuItem class defines the menu items and MenuArrayAdapter inherits from ArrayAdapter to hold an array of menu items.
The 3 layouts include menu list layout, add item layout and veggie/non-veggie toggle layout.
I have considered an JSON input for receiving the menu from the server. I have used some partial codes (No copyright infringement) 
from online resources. I received some pointers to provide clean, release-ready code. 

Missing requirements:
    * Toggle button for the food type filtering does not work properly  
    * Assigning unique IDs to orders, including relation to device ID for delivery and transfer purposes of the order.
    * Multi location services is not clear, currently it is not supported. To enable we need addition of DB/API
    schemas to relate with locations and users. 
    * Test cases 
        
For enabling multi user access scenario need to add the following:
    * Users: creating, deleting, editing
    * Auth
    * Different UIs for different access levels
    * other views for login (login, log out, unable to login)
    
Test cases to consider. Please note the success or failure condition of these test cases change based on project requirements:
    * Status of menu items after submitting orders 
    * Checking edge cases for item add text box: empty string, very long string, duplicated string
    * Verify correctness and accuracy of order summary: items match, empty order, very long order, etc.
    * Checking functionality of the veggie toggle switches in the main dialog: list updates correctly, persistency of the selection, etc.
    * Checking if the add item works properly: check sync with server, item adds correctly, the status of veggie/non-veggie
    * Check for different device resolutions

Libraries used:
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import java.util.ArrayList;
import android.content.res.Resources;
import android.support.v4.content.res.ResourcesCompat;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import java.util.ArrayList;
import org.json.JSONArray ;
import org.json.JSONObject;
